export const PackageType = {
  CONAN: 'conan',
  MAVEN: 'maven',
  NPM: 'npm',
};

export const TrackingLabels = {
  CODE_INSTRUCTION: 'code_instruction',
  MAVEN_INSTALLATION: 'maven_installation',
  NPM_INSTALLATION: 'npm_installation',
};

export const TrackingActions = {
  INSTALLATION: 'installation',
  REGISTRY_SETUP: 'registry_setup',

  COPY_MAVEN_XML: 'copy_maven_xml',
  COPY_MAVEN_COMMAND: 'copy_maven_command',
  COPY_MAVEN_SETUP: 'copy_maven_setup_xml',

  COPY_NPM_INSTALL_COMMAND: 'copy_npm_install_command',
  COPY_NPM_SETUP_COMMAND: 'copy_npm_setup_command',

  COPY_YARN_INSTALL_COMMAND: 'copy_yarn_install_command',
  COPY_YARN_SETUP_COMMAND: 'copy_yarn_setup_command',
};
